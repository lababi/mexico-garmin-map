### README ###

This script downloads the latest version of OpenStreetMap (OSM) data of Mexico and elevation lines (NASA SRTM) to create a gmapsupp.img file, which provides the map on Garmin devices. The resulting map is ~1.3 Gb (9 of June 2016) and works fine on an etrex Vista HCx. The cyclemap style included is from http://wiki.openstreetmap.org/wiki/OSM_Map_On_Garmin/Cycle_map.

### INSTALLATION ###

The script works on a bash shell, with java, python, wget and bzip2 installed. 
Further, you need some special programs:
phyghtmap: http://katze.tfiu.de/projects/phyghtmap/
tile splitter: http://www.mkgmap.org.uk/doc/splitter.html
mkgmap: http://www.mkgmap.org.uk/

### MEXICO GARMIN MAP DOWNLOAD ###
You can download a ready gmapsupp.img (OSM data of 22 of June, 2016) file for your Garmin device from <a href="https://doi.org/10.5281/zenodo.494705"></a>


### CONTACT ###

robert.winkler@bioprocess.org