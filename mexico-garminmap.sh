wget -O mexico-latest.osm.bz2 "http://download.geofabrik.de/north-america/mexico-latest.osm.bz2"
bzip2 -d mexico-latest.osm.bz2 
java -Xmx1024m -jar /home/rob/GPS/splitter-r437/splitter.jar --max-nodes=900000 mexico-latest.osm
phyghtmap -a -124.849:13.197:-80.200:33.797 -o mexico_contours -0 --source=view3 --pbf
java -Xmx1024m -jar /home/rob/GPS/mkgmap-r3676/mkgmap.jar --style-file=cyclemap --route -c template.args --gmapsupp cyclemap.TYP mexico_contours*pbf
